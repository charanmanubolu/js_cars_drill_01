/* 
// ==== Problem #6 ====
// A buyer is interested in seeing only BMW and Audi cars within the inventory. 
 Execute a function and return an array that only contains BMW and Audi cars.  
 Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console.
*/
function Problem6(arr) {
    let result =[]
    if(Array.isArray(arr)){
    for(let index=0; index<arr.length ; index++){
        if(arr[index].car_make ==="Audi" ||arr[index].car_make ==="BMW" ){
            result.push(arr[index])
        }
    }
}else {
    throw new Error(Error);
}
    return result;
    
}
module.exports=Problem6;