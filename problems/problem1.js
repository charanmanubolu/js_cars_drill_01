// ==== Problem #1 ====
// The dealer can't recall the information for a car with an id of 33 on his lot. 
//Help the dealer find out which car has an id of 33 by calling a function that will return the data for that car.
// Then log the car's year, make, and model in the console log in the format of:
//"Car 33 is a *car year goes here* *car make goes here* *car model goes here*"

function problem1(arr, id) {
    if (Array.isArray(arr) && typeof(id) === "number") {

        for (let index = 0; index < arr.length; index++) {
            if (arr[index].id === id) {
                console.log(`Car ${arr[index].id} is a ${arr[index].car_year} , ${arr[index].car_make} and ${arr[index].car_model}`)
            }
        }
    } else {
        console.log([])
    }
}
module.exports = problem1


