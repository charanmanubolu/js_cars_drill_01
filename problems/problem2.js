/*
// ==== Problem #2 ====
// The dealer needs the information on the last car in their inventory.
 Execute a function to find what the make and model of the last car in the inventory is?  
 Log the make and model into the console in the format of:
"Last car is a *car make goes here* *car model goes here*"
*/



function problem2(arr ) {
    if(Array.isArray(arr) ){
    for(let index=0;index<arr.length;index++){
    if(arr[index].id===arr.length){
        console.log(`Last car is a ${arr[index].car_make} and ${arr[index].car_model}`)
    }
}
    }else{
        throw new Error(Error)
    }
    
}
module.exports = problem2

