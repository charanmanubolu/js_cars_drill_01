/* // ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all 
the car model names into alphabetical order and log the results in the console as it was returned.
*/

function problem3(arr) {
   const car_model = []
   if (Array.isArray(arr)) {

      for (let index = 0; index < arr.length; index++) {
         car_model.push(arr[index].car_model)
      }
   } else {
      throw new Error(Error)
   }
   return car_model.sort()
}

module.exports = problem3