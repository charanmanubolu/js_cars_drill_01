/*
// ==== Problem #5 ====
// The car lot manager needs to find out how many cars are older than the year 2000.
 Using the array you just obtained from the previous problem, 
 find out how many cars were made before the year 2000 and return the array of older cars and log its length.

 */

 function problem5(arr){
    let result =[]
    if(Array.isArray(arr)){
    for(let index=0 ; index<arr.length ; index++){
        if(arr[index] < 2000){
            result.push(arr[index])
        }
    }
}else{
    throw new Error(Error)
}
    return result;

 }

 module.exports = problem5